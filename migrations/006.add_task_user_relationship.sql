CREATE TABLE task_user_relations (
    task_id varchar(32) NOT NULL,
    user_id varchar(32) NOT NULL,
    PRIMARY KEY (task_id, user_id)
);
INSERT INTO migrations (version) VALUES (6);
