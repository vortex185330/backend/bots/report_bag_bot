import os
from typing import Optional

import aiopg
from psycopg2.extras import DictCursor

from models import Post
import logging

# console_handler = logging.StreamHandler()
# console_handler.setLevel(logging.INFO)
# formatter = logging.Formatter("%(asctime)s %(levelname)s | [%(name)s] %(message)s")
# console_handler.setFormatter(formatter)
# logging.getLogger().addHandler(console_handler)

logger = logging.getLogger(__name__)


def build_dsn():
    dbname = os.getenv('DB_NAME', default="main")
    user = os.getenv('DB_USER', default="postgres")
    password = os.getenv('DB_PASSWORD', default="")
    host = os.getenv('DB_HOST', default="localhost")
    port = os.getenv('DB_PORT', default="5432")
    return f"dbname={dbname} user={user} password={password} host={host} port={port}"


class ConnectionManager:
    _instance: Optional['ConnectionManager'] = None
    _connection: aiopg.Connection | None = None

    def __new__(cls, *args, **kwargs):
        if cls._instance is None:
            cls._instance = super().__new__(cls, *args, **kwargs)
        return cls._instance

    @property
    def connected(self) -> bool:
        return self._connection is not None and not self._connection.closed

    async def connection(self) -> aiopg.Connection:
        return await self.__aenter__()

    async def __aenter__(self) -> aiopg.Connection:
        if not self.connected:
            self._connection = await aiopg.connect(build_dsn(), cursor_factory=DictCursor)

        return self._connection

    async def __aexit__(self, exc_type, exc_val, exc_tb):
        if self.connected:
            await self._connection.close()


async def get_user_vote(message_id: int | str, user_id: int | str) -> str | None:
    stmt = """
    SELECT vote FROM votes WHERE (message_id, user_id) = (%(message_id)s, %(user_id)s);
    """
    params = {
        "message_id": str(message_id),
        "user_id": str(user_id)
    }

    conn = await ConnectionManager().connection()
    async with conn.cursor() as cur:
        await cur.execute(stmt, params)
        result = await cur.fetchone()

    return result[0] if result else None


async def get_user_id_by_task_id(task_id: str) -> str | None:
    """Get user_id associated with a ClickUp task_id"""

    stmt = """
    SELECT user_id FROM task_user_relations WHERE task_id = %(task_id)s LIMIT 1;
    """

    params = {
        "task_id": task_id,
    }

    conn = await ConnectionManager().connection()
    async with conn.cursor() as cur:
        await cur.execute(stmt, params)
        result = await cur.fetchone()
        return result[0] if result else None


async def get_rating(message_id: int | str) -> tuple[int, int]:
    stmt = """
    SELECT vote, COUNT(vote) FROM votes WHERE message_id = %(message_id)s GROUP BY vote;
    """

    params = {
        "message_id": str(message_id),
    }

    conn = await ConnectionManager().connection()
    async with conn.cursor() as cur:
        await cur.execute(stmt, params)
        result = await cur.fetchall()

    result = dict(result)
    return result.get("+", 0), result.get("-", 0)


async def add_post(
        message_id: int | str,
        user_id: int | str,
        thread_id: int | str,
        media_group: str | None = None
):
    """Save post information"""

    stmt = """
    INSERT INTO posts (message_id, user_id, date, comment_thread_id, media_group) 
    VALUES (%(message_id)s, %(user_id)s, now(), %(thread_id)s, %(media_group)s);
    """

    params = {
        "message_id": str(message_id),
        "user_id": str(user_id),
        "thread_id": str(thread_id),
        "media_group": media_group
    }

    conn = await ConnectionManager().connection()
    async with conn.cursor() as cur:
        await cur.execute(stmt, params)


async def save_task_user_relation(task_id: str, user_id: int | str):
    """Save relation between ClickUp task and Telegram user"""

    stmt = """
    INSERT INTO task_user_relations (task_id, user_id) 
    VALUES (%(task_id)s, %(user_id)s);
    """

    params = {
        "task_id": str(task_id),
        "user_id": str(user_id)
    }

    conn = await ConnectionManager().connection()
    async with conn.cursor() as cur:
        await cur.execute(stmt, params)


async def get_post(message_id: int | str) -> Post:
    """Fetch post"""

    stmt = """
    SELECT message_id, user_id, date, comment_thread_id, comments, popular_id, media_group
    FROM posts WHERE message_id = %(message_id)s;
    """

    params = {
        "message_id": str(message_id),
    }

    conn = await ConnectionManager().connection()
    async with conn.cursor() as cur:
        await cur.execute(stmt, params)
        result = await cur.fetchone()

    return Post(**result) if result else None


async def get_post_by_media_group(media_group: str) -> Post:
    """Fetch post by group_id"""

    stmt = """
    SELECT message_id, user_id, date, comment_thread_id, comments, popular_id, media_group
    FROM posts WHERE media_group = %(media_group)s;
    """

    params = {
        "media_group": media_group,
    }

    conn = await ConnectionManager().connection()
    async with conn.cursor() as cur:
        await cur.execute(stmt, params)
        result = await cur.fetchone()

    return Post(**result) if result else None


async def increase_comments_counter(thread_id: int | str) -> Post:
    """Increases comments counter by 1"""

    stmt = """
    UPDATE posts SET comments = comments + 1 WHERE comment_thread_id = %(thread_id)s
    RETURNING message_id, user_id, date, comment_thread_id, comments, popular_id, media_group;
    """

    params = {
        "thread_id": str(thread_id),
    }

    conn = await ConnectionManager().connection()
    async with conn.cursor() as cur:
        await cur.execute(stmt, params)
        result = await cur.fetchone()

    return Post(**result) if result else None
