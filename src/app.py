import logging
import os
import threading

import flask
import requests
from telegram import Update
from telegram.ext import (
    ApplicationBuilder,
    CommandHandler,
    MessageHandler,
    CallbackContext,
    filters,
)

import db
from config import (
    CHAT_ID_NEW, 
    COMMENTS_GROUP_ID,
    TOKEN,
    WELCOME_TEXT,
    CHECK_CHANNEL_NAME,
    CLICKUP_API_KEY,
    CLICKUP_LIST_ID
)
from models import PostKeyboard

# Set up flask app
flask_app = flask.Flask(__name__)

# Set up logging to a file and console
LOG_FILE = os.getenv('BUG_REPORT_LOG_FILE', default='bot.log')
logging.basicConfig(
    filename=LOG_FILE,
    format="%(asctime)s %(levelname)s | [%(name)s] %(message)s",
    level=logging.INFO
)
logging.getLogger("httpx").setLevel(logging.ERROR)

# Create a new console handler
console_handler = logging.StreamHandler()
console_handler.setLevel(logging.INFO)
formatter = logging.Formatter("%(asctime)s %(levelname)s | [%(name)s] %(message)s")
console_handler.setFormatter(formatter)

# Add the console handler to the root logger
logging.getLogger().addHandler(console_handler)

logger = logging.getLogger(__name__)


@flask_app.route('/healthz', methods=['GET'])
def healthcheck() -> tuple[str, int]:
    """Health check route for the Flask web application."""
    return 'Health check successful', 200

async def start(update: Update, _):
    """Handler for the /start command."""
    user_id: int = update.message.from_user.id

    if not await check_subscription(user_id, _):
        await update.message.reply_text(
            "Для отправки багрепортов, пожалуйста, подпишитесь на наш канал: " + CHECK_CHANNEL_NAME
        )
        return  # Прекратить обработку, если пользователь не подписан
    await update.message.reply_text(WELCOME_TEXT)


async def check_subscription(user_id: int, context: CallbackContext) -> bool:
    try:
        chat_member = await context.bot.get_chat_member(chat_id=CHECK_CHANNEL_NAME, user_id=user_id)
        return chat_member.status not in ['left', 'kicked']
    except Exception as e:
        logger.error(f"Error checking subscription for user {user_id}: {e}")
        return False  # Возвращаем False, если возникла ошибка


async def validate_message(text: str) -> bool:
    # Здесь вы можете добавить свои правила валидации
    # Например, проверить, есть ли в тексте определенные ключевые слова или фразы
    expected_phrases = ["ожидаемый результат", "фактический результат"]
    return all(phrase in text.lower() for phrase in expected_phrases)

def create_clickup_task(name: str, task_description: str):
    task_name = f"БР от {name}: "f"{extract_task_title(task_description)}"
    API_KEY = CLICKUP_API_KEY
    LIST_ID = CLICKUP_LIST_ID
    headers = {
        'Authorization': API_KEY
    }
    data = {
        'name': task_name,
        'description': task_description,
    }

    response = requests.post(f'https://api.clickup.com/api/v2/list/{LIST_ID}/task',
                             headers=headers, json=data)
    if response.status_code == 200:
        task_id = response.json()['id']
        return task_id
    return None


def extract_task_title(text: str) -> str:
    # Разбиваем текст на предложения
    sentences = text.split('.')
    first_sentence = sentences[0].strip()

    # Если первое предложение короче 100 символов, возвращаем его
    if len(first_sentence) <= 100:
        return first_sentence
    # Иначе возвращаем первые 100 символов текста
    return text[:100]


async def media_handler(update: Update, context: CallbackContext) -> None:
    """Handler for media files (photos)."""
    user_id: int = update.message.from_user.id

    if not await check_subscription(user_id, context):
        await update.message.reply_text(
            "Для отправки багрепортов, пожалуйста, подпишитесь на наш канал: " + CHECK_CHANNEL_NAME
        )
        return  # Прекратить обработку, если пользователь не подписан

    media_message = update.message
    caption: str | None = media_message.caption

    # Check the user's post count for today in the database

    media_group = media_message.media_group_id
    if media_group is not None:
        post = await db.get_post_by_media_group(media_group)
        if post is not None:
            if media_message.photo:
                await context.bot.send_photo(
                    photo=update.message.photo[-1],
                    chat_id=COMMENTS_GROUP_ID,
                    reply_to_message_id=int(post["comment_thread_id"]),
                )
            elif media_message.video:
                await context.bot.send_video(
                    video=media_message.video,
                    chat_id=COMMENTS_GROUP_ID,
                    reply_to_message_id=int(post["comment_thread_id"]),
                )
            else:
                logger.error(f"Unknown media type in message {media_message}")
            return

    user_name = update.message.from_user.first_name  # Get user's first name
    username = update.message.from_user.username  # Get user's username

    name = f"@{username}" if username else user_name
    user_signature = f"{name}\n{caption}\n" if caption else f"{name}"

    if media_message.photo:
        msg = await context.bot.send_photo(
            chat_id=CHAT_ID_NEW,
            photo=media_message.photo[-1],
            caption=user_signature,
        )
    elif media_message.video:
        msg = await context.bot.send_video(
            chat_id=CHAT_ID_NEW,
            video=media_message.video,
            caption=user_signature,
        )
    else:
        logger.error(f"Unknown media type in message {media_message}")
        return

    chat_id_without_minus = str(CHAT_ID_NEW).replace("-100", "")  # Убираем "-100" из ID чата
    telegram_link = f"https://t.me/c/{chat_id_without_minus}/{msg.message_id}"

    description = caption if caption else ""
    description += f"\n\n В багрепорте есть медиа. \n\nСсылка на багрепорт: {telegram_link}"

    task_id = create_clickup_task(name, description)
    if task_id:
        await db.save_task_user_relation(task_id, user_id)

    thread = await msg.copy(COMMENTS_GROUP_ID, disable_notification=True)
    await context.bot.pin_chat_message(COMMENTS_GROUP_ID, thread.message_id)

    await db.add_post(msg.message_id, user_id, thread.message_id, media_group)

    if caption and await validate_message(caption):
        await post_feedback(update, msg.message_id, task_id)
    else:
        await update.message.reply_text(
            f"Твой багрепорт у нас, id: {task_id}! 🚀 Но, пожалуйста, оформи его по форме: "
            "ожидаемый результат, фактический результат... 🌺"
        )

    logger.info(f"Created new post {msg.message_id} by user {username}")


async def message_handler(update: Update, context: CallbackContext):
    user_id = update.message.from_user.id
    if not await check_subscription(user_id, context):
        await update.message.reply_text(
            "Для отправки багрепортов, пожалуйста, подпишитесь на наш канал: " + CHECK_CHANNEL_NAME
        )
        return  # Прекратить обработку, если пользователь не подписан

    # Check the user's post count for today in the database

    username = update.message.from_user.username
    user_name = update.message.from_user.first_name
    caption = update.message.caption
    name = f"@{username}" if username else user_name
    user_signature = f"{name}\n{caption}\n" if caption else f"{name}"
    content = f"{user_signature}\n{update.message.text}"
    msg = await context.bot.send_message(CHAT_ID_NEW, content)

    chat_id_without_minus = str(CHAT_ID_NEW).replace("-100", "")  # Убираем "-100" из ID чата
    telegram_link = f"https://t.me/c/{chat_id_without_minus}/{msg.message_id}"

    description = update.message.text if update.message.text else ""
    description += f"\n\nСсылка на багрепорт: {telegram_link}"

    task_id = create_clickup_task(name, description)
    if task_id:
        await db.save_task_user_relation(task_id, user_id)

    thread = await msg.copy(COMMENTS_GROUP_ID, disable_notification=True)
    await context.bot.pin_chat_message(COMMENTS_GROUP_ID, thread.message_id)

    await db.add_post(msg.message_id, user_id, thread.message_id)

    if await validate_message(update.message.text):
        await post_feedback(update, msg.message_id, task_id)
    else:
        await update.message.reply_text(
            f"Твой багрепорт у нас, id {task_id}! 🚀 Но, пожалуйста, оформи его по форме: "
            "ожидаемый результат, фактический результат... 🌺"
        )

    logger.info(f"Created new post {msg.message_id} by user {update.message.from_user.username}")


async def comments_handler(update: Update, context: CallbackContext):
    """Handler for user comments"""

    thread_id = update.message.message_thread_id
    if not thread_id or update.message.pinned_message:
        return

    logger.info(f"User {update.message.from_user.username} left a comment in {thread_id} thread")

    post = await db.increase_comments_counter(thread_id)
    rating = await db.get_rating(post["message_id"])
    keyboard = PostKeyboard(
        rating=rating[0] - rating[1],
        thread_id=post["comment_thread_id"],
        comments=post["comments"]
    )

    await context.bot.edit_message_reply_markup(
        chat_id=CHAT_ID_NEW,
        message_id=int(post["message_id"]),
        reply_markup=keyboard.to_reply_markup()
    )


async def post_feedback(update: Update, id_report: int, task_id: str):
    await update.message.reply_text(
        f"Ваш багрепорт добавлен! Его номер {id_report}, его id {task_id} \n"
    )


def main():
    application = (
        ApplicationBuilder()
        .token(TOKEN)
        .connect_timeout(10)  # default 5s
        .read_timeout(30)  # default 5s
        .write_timeout(30)  # default 5s
        .get_updates_connect_timeout(60)  # default 5s
        .get_updates_pool_timeout(60)  # default 1s
        .get_updates_read_timeout(60)  # default 5s
        .get_updates_write_timeout(60)  # default 5s
        .pool_timeout(10)  # default 1s
        .build()
    )

    application.add_handler(CommandHandler("start", start, filters=filters.ChatType.PRIVATE))
    application.add_handler(MessageHandler(~filters.COMMAND & filters.TEXT & filters.ChatType.PRIVATE, message_handler))
    application.add_handler(
        MessageHandler(~filters.COMMAND & (filters.PHOTO | filters.VIDEO) & filters.ChatType.PRIVATE, media_handler))
    # application.add_handler(CallbackQueryHandler(vote_handler))
    application.add_handler(MessageHandler(~filters.COMMAND & filters.Chat(int(COMMENTS_GROUP_ID)), comments_handler))

    application.run_polling(
        allowed_updates=[
            Update.MESSAGE,
            Update.CALLBACK_QUERY,
        ]
    )


if __name__ == '__main__':
    flask_thread = threading.Thread(target=flask_app.run, kwargs={"host": "0.0.0.0", "port": 8080})
    flask_thread.start()
    main()
