import enum
from datetime import datetime
from typing import TypedDict

from telegram import InlineKeyboardMarkup


class Post(TypedDict):
    message_id: str
    user_id: str
    date: datetime
    comment_thread_id: str
    comments: int
    popular_id: str
    media_group: str


class PostKeyboard:

    def __init__(
            self,
            *,
            rating: int = 0,
            comments: int = 0,
            thread_id: int | str = None
    ):
        self.rating = rating
        self.comments = comments
        self.thread_id = thread_id

    def to_reply_markup(self) -> InlineKeyboardMarkup:
        return None
