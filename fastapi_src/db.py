import os
from typing import Optional

import aiopg
from psycopg2.extras import DictCursor

from models import Post
import logging
# console_handler = logging.StreamHandler()
# console_handler.setLevel(logging.INFO)
# formatter = logging.Formatter("%(asctime)s %(levelname)s | [%(name)s] %(message)s")
# console_handler.setFormatter(formatter)
# logging.getLogger().addHandler(console_handler)

logger = logging.getLogger(__name__)

def build_dsn():

    dbname = os.getenv('DB_NAME', default="main")
    user = os.getenv('DB_USER', default="postgres")
    password = os.getenv('DB_PASSWORD', default="")
    host = os.getenv('DB_HOST', default="localhost")
    port = os.getenv('DB_PORT', default="5432")
    return f"dbname={dbname} user={user} password={password} host={host} port={port}"


class ConnectionManager:
    _instance: Optional['ConnectionManager'] = None
    _connection: aiopg.Connection | None = None

    def __new__(cls, *args, **kwargs):
        if cls._instance is None:
            cls._instance = super().__new__(cls, *args, **kwargs)
        return cls._instance

    @property
    def connected(self) -> bool:
        return self._connection is not None and not self._connection.closed

    async def connection(self) -> aiopg.Connection:
        return await self.__aenter__()

    async def __aenter__(self) -> aiopg.Connection:
        if not self.connected:
            self._connection = await aiopg.connect(build_dsn(), cursor_factory=DictCursor)

        return self._connection

    async def __aexit__(self, exc_type, exc_val, exc_tb):
        if self.connected:
            await self._connection.close()


async def get_user_id_by_task_id(task_id: str) -> str | None:
    """Get user_id associated with a ClickUp task_id"""

    stmt = """
    SELECT user_id FROM task_user_relations WHERE task_id = %(task_id)s LIMIT 1;
    """

    params = {
        "task_id": task_id,
    }

    conn = await ConnectionManager().connection()
    async with conn.cursor() as cur:
        await cur.execute(stmt, params)
        result = await cur.fetchone()
        return result[0] if result else None


