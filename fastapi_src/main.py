import asyncio
import logging
import os

import httpx
import uvicorn
from fastapi import FastAPI, Request

from config import (
    TOKEN, WEBHOOK_PATH
)
from db import get_user_id_by_task_id

app = FastAPI()

# Set up logging to a file and console
LOG_FILE = os.getenv('BUG_REPORT_LOG_FILE', default='bot.log')
logging.basicConfig(
    filename=LOG_FILE,
    format="%(asctime)s %(levelname)s | [%(name)s] %(message)s",
    level=logging.INFO
)
logging.getLogger("httpx").setLevel(logging.ERROR)

# Create a new console handler
console_handler = logging.StreamHandler()
console_handler.setLevel(logging.INFO)
formatter = logging.Formatter("%(asctime)s %(levelname)s | [%(name)s] %(message)s")
console_handler.setFormatter(formatter)

# Add the console handler to the root logger
logging.getLogger().addHandler(console_handler)

logger = logging.getLogger(__name__)


@app.get("/")
async def read_root():
    return {"Hello": "World"}


@app.get("/healthz")
def healthcheck() -> tuple[str, int]:
    """Health check route for the Flask web application."""
    return 'Health check successful', 200


@app.post(WEBHOOK_PATH)
async def clickup_webhook(request: Request):
    logger.info("Webhook called")
    data = await request.json()
    print(data)
    task_id = data.get('task_id')
    if not task_id:
        logger.error(f"Unrecognized response {data}")
        return {"message": "Unrecognized response"}
    user_id = await get_user_id_by_task_id(task_id)
    if not user_id:
        logger.info(f'For task_id {task_id} does not found user')
        return
    event = data.get('event')
    if event not in ['taskStatusUpdated', 'taskCommentPosted']:
        logger.error(f"Unrecognized event. Event: {event}, response: {data}.")
        return
    history_items = data.get('history_items')
    if not history_items or len(history_items) != 1:
        logger.error(f'Unrecognized history_items: len(hi): {len(history_items)}')
        return
    task_detail = history_items[0]

    if event == 'taskStatusUpdated':
        task_status_before = task_detail.get('before', {}).get('status')
        task_status_after = task_detail.get('after', {}).get('status')
        message = (f'Задача созданная по вашему багрепорту {task_id} '
                   f'переведена из статуса {task_status_before} в статус {task_status_after}')
        await send_telegram_message(user_id, message)

        return
    if event == 'taskCommentPosted':
        comment = task_detail.get('comment', {}).get('text_content', '')
        if not comment:
            logger.error(f'Comment not found')
            return
        if 'ответ пользователю:' not in comment.lower():
            logger.info(f'This is not comment for user: {comment}')
            return
        message = (f'В задаче созданной по вашему багрепорту {task_id} '
                   f'оставлен комментарий для вас: {comment}')
        await send_telegram_message(user_id, message)

        return


async def send_telegram_message(chat_id, text):
    url = f"https://api.telegram.org/bot{TOKEN}/sendMessage"
    data = {
        "chat_id": chat_id,
        "text": text
    }
    async with httpx.AsyncClient() as client:
        response = await client.post(url, data=data)
    return response


# Запуск FastAPI
def run_api():
    import uvicorn
    uvicorn.run(app, host="0.0.0.0", port=8000)


# Асинхронная функция для запуска сервера FastAPI с помощью Uvicorn
async def run_api():
    config = uvicorn.Config(app=app, host="0.0.0.0", port=8000)
    server = uvicorn.Server(config)
    await server.serve()


if __name__ == "__main__":
    asyncio.run(run_api())

