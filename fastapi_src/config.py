import os

TOKEN = os.getenv("TELEGRAM_BOT_TOKEN")
CHAT_ID_NEW = os.getenv("TELEGRAM_CHANNEL_ID")
COMMENTS_GROUP_ID = os.getenv("TELEGRAM_COMMENTS_GROUP_ID")
CHECK_CHANNEL_NAME = os.getenv("CHECK_CHANNEL_NAME")
WEBHOOK_PATH = os.getenv("WEBHOOK_PATH")

CLICKUP_API_KEY = os.getenv("CLICKUP_API_KEY")
CLICKUP_LIST_ID = os.getenv("CLICKUP_LIST_ID")

_default_welcome_text = """
"Привет, Капибарин/Капибарышня!
🌟 Чтобы сообщить о проблеме, возникшей на kapi.bar, просто напиши мне.
Вот короткая инструкция, как это сделать: https://t.me/alpha_kapibara/2/5"


КАК ОТПРАВИТЬ БАГРЕПОРТ
1. Отправьте сообщение боту и он перешлет его в канал к разработчикам
2. К сообщению можно прикрепить одно изображение или одно видео
3. Некоторые возможности телеграм не поддерживаются на данный момент ботом: нельзя добавить стиль тексту жирный, подчеркнутый, зачеркнутый, курсив и так далее.
4. После публикации ваш пост появится на канале для сбора багрепортов

"""
WELCOME_TEXT = os.getenv("WELCOME_TEXT", _default_welcome_text)
