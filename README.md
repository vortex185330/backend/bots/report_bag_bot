# Bot
Set up your config in .env or prod.env and run

### Env file
```env
# Telegram vars
TELEGRAM_BOT_TOKEN=xxx
TELEGRAM_CHANNEL_ID=-100id
TELEGRAM_COMMENTS_GROUP_TAG=@mygroup

# Logging
LOG_FILE=./bug_report_logfile.log

# Postgres vars
POSTGRES_DB=main2
POSTGRES_PASSWORD=postgres2
DB_PASSWORD=postgres2

# DB config
DB_NAME = main2
DB_USER = postgres2
DB_PASSWORD = postgres2
DB_HOST = db2
DB_PORT = 5432

# How many posts per day user can publish
# Positive votes percentage threshold for post to become popular
# Number of positive votes for post to become popular (applies together with the above)

WELCOME_TEXT = """
Добро пожаловать в бот канала Капибара Новое! 

Здесь каждый может поделиться своими творческими историями, фотографиями и другими удивительными моментами. Канал основного проекта t.me/new_old_pikabu 

Когда ваш пост откликнется у других пользователей он появится на канале Капибара Популярное https://t.me/best_kapibara
Добавлять посты через этого бота: @ContentAddBot

Используйте пожалуйста те же #теги которые были на пкб.
После публикации ваш пост появится на https://t.me/new_kapibara. В данный момент доступны для публикации фото, видео, тест и теги

Навигация по проекту: https://t.me/new_old_pikabu/83517/83518
"""
```

### Run
`docker-compose up --build`